from __future__ import unicode_literals
from django.db import connection
from django.db import models

from django.contrib.auth.models import User

class Mahasiswa(models.Model):
        JK_CHOICES = (('l','Pria'),('w','Wanita'))
        PS_CHOICES = (('mi','Managemen Informatika'),('ak','Akuntansi'),
                      ('me','Mesin'))
        nama = models.CharField('nama mahasiswa',max_length=50)
        alamat = models.CharField(max_length=100)
        jk = models.CharField('kelamin',max_length=1, choices=JK_CHOICES)
        ps = models.CharField('program studi',max_length=2, choices=PS_CHOICES)
        nim = models.CharField(max_length=30)
        no_tlp = models.CharField('no.hp',max_length=30, blank=True)
        email = models.CharField('email',max_length=100, blank=True)
        def __unicode__(self):
                return self.nama
	
