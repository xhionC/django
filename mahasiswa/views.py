from django.shortcuts import render
from . import models

dataMhs = models.Mahasiswa.objects.all()

context = { 'judul':'Web Mahasiswa',
			'pagename':'Halaman Tampil Mahasiswa',
			'kontributor':'Aditya Nugraha',
			'Mahasiswas':dataMhs}

def index(request):
	return render(request,'mahasiswa/index.html',context)
